//Includes
#include "SwcHeadlight.h"
#include <Utility.h>
#include"Dio.h"

#define DUTY_100_PERCENT 1000

#define VOLTAGE_MIN 0  //This is board dependent. 5V is assumed for STM3210C
#define VOLTAGE_MAX 5  //This is board dependent. 5V is assumed for STM3210C
#define VOLTAGE_RANGE (VOLTAGE_MAX-VOLTAGE_MIN)

#define MIN_VALUE 0//((DUTY_100_PERCENT*1)/20)
#define MAX_VALUE DUTY_100_PERCENT//((DUTY_100_PERCENT*2)/20)
#define RANGE (MAX_VALUE-MIN_VALUE)


void ServoMotorControllerFunction(const float64 *headLightState,
									DutyCycle* pwmOutput) {
     /*TODO: Use value of voltage to control the angle.
        https://learn.sparkfun.com/tutorials/pulse-width-modulation 
         Frequency/period are specific to controlling a specific servo. 
         * A typical servo motor expects to be updated every 20 ms with
         * a pulse between 1 ms and 2 ms, or in other words, between a 
         * 5 and 10% duty cycle on a 50 Hz waveform. With a 1.5 ms 
         * pulse, the servo motor will be at the natural 90 degree 
         * position. With a 1 ms pulse, the servo will be at the 0 
         * degree position, and with a 2 ms pulse, the servo will be 
         * at 180 degrees. You can obtain the full range of motion by 
         * updating the servo with an value in between.             
         */
    //*pwmOutput = 0.25 + (*headLightState)/20;
    /* Assumption" *headLightState can have integer values from 0-5.
     * The sine wave is all + and has a amplitude of +5.    */

	*pwmOutput = DUTY_100_PERCENT;
	float64 temp = *headLightState;
	
	if(temp > VOLTAGE_MAX) {
		temp =VOLTAGE_MAX;
	}
	else if(temp  < VOLTAGE_MIN) {
		temp =VOLTAGE_MIN;
	}
	else{
		//temp = VOLTAGE_MIN+VOLTAGE_RANGE/2.0; //was used in experiment.
	}

	temp = MIN_VALUE + ((temp*RANGE) / VOLTAGE_RANGE);;
		
	*pwmOutput = (DutyCycle) temp;
}



//Runnables
void swcHeadlightMainRunnable(void){
	//WRITE_EVENT_TIMESTAMP(Event_9);

	SignalQuality quality = 0;
	DutyCycle dutyCycle = 0; 
	float64 headLightState = 0.0;
	Std_ReturnType ret = E_NOT_OK;

	//WRITE_EVENT_TIMESTAMP(Event_9);
	ret = Rte_Read_lightBrightnessReceiver_lightBrightness(/*OUT*/ &headLightState);
	//WRITE_EVENT_TIMESTAMP(Event_10);
	if(RTE_E_OK != ret)
		;//report error
	
	if(BEAM_HIGH == headLightState)
		headLightState = VOLTAGE_MAX;
	else if(BEAM_LOW == headLightState)
		headLightState = (VOLTAGE_RANGE/2.0);
	else
		headLightState = VOLTAGE_MIN;
	
	ServoMotorControllerFunction(&headLightState, &dutyCycle);

	//WRITE_EVENT_TIMESTAMP(Event_10);
	Rte_Call_HeadlightPwmDuty_Set(dutyCycle, &quality);
	//WRITE_EVENT_TIMESTAMP(Event_11);
	if(RTE_E_PwmServiceSetDuty_E_OK != ret){
		;//report error
	}
	//WRITE_EVENT_TIMESTAMP(Event_11);
			/*while(true){
			//do nothing
			;
		}*/
}



/* Model initialize function */
void swcHeadlightInitRunnable(void){
  /* (no initialization code required) */
}



