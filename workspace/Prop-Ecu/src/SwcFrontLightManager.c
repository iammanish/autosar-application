#include "SwcFrontLightManager.h"
#include <stdlib.h>
#include "Utility.h"
#include "Dio.h"



void turnOtherLightOn(LightRequestRecord* data){
	if(!data)
		return;//report error

	//here if else ladder can be used if blinking is visible even after that	
	if(data->off){
			////////WRITE_EVENT_TIMESTAMP(Event_9);
			Rte_Call_ParkingLightClient_Write(STD_LOW);
			////////WRITE_EVENT_TIMESTAMP(Event_9);
			Rte_Call_LeftIndicatorClient_Write(STD_LOW);
			////////WRITE_EVENT_TIMESTAMP(Event_9);
			Rte_Call_RightIndicatorClient_Write(STD_LOW);
			//////WRITE_EVENT_TIMESTAMP(Event_9);
	}
	if(data->leftIndicator){
		//////WRITE_EVENT_TIMESTAMP(Event_9);
		Rte_Call_LeftIndicatorClient_Write(STD_HIGH);
		//////WRITE_EVENT_TIMESTAMP(Event_9);
	}	
	if(data->rightIndicator){
		//////WRITE_EVENT_TIMESTAMP(Event_9);
		Rte_Call_RightIndicatorClient_Write(STD_HIGH);
		//////WRITE_EVENT_TIMESTAMP(Event_9);
	}
	if(data->parking){
		//////WRITE_EVENT_TIMESTAMP(Event_9);
		Rte_Call_ParkingLightClient_Write(STD_HIGH);
		//////WRITE_EVENT_TIMESTAMP(Event_9);
	}
}


//right now blink is used as a dummy mode.
void handleBlinkState(e_BlinkingMode blinkState){
	switch(blinkState){
	case BLINK_ON:
		////////WRITE_EVENT_TIMESTAMP(Event_13);
		//////WRITE_EVENT_TIMESTAMP(Event_9);
		Rte_Call_ParkingLightClient_Write(STD_HIGH);
		//////WRITE_EVENT_TIMESTAMP(Event_9);
		////////WRITE_EVENT_TIMESTAMP(Event_13);
		////////WRITE_EVENT_TIMESTAMP(Event_14);
		Rte_Call_LeftIndicatorClient_Write(STD_HIGH);
		////////WRITE_EVENT_TIMESTAMP(Event_14);
		Rte_Call_RightIndicatorClient_Write(STD_HIGH);
		break;  //TODO: remove this break to enable blink
	case BLINK_OFF:
		////////WRITE_EVENT_TIMESTAMP(Event_12);
		//////WRITE_EVENT_TIMESTAMP(Event_9);
		Rte_Call_ParkingLightClient_Write(STD_LOW);
		//////WRITE_EVENT_TIMESTAMP(Event_9);
		////////WRITE_EVENT_TIMESTAMP(Event_12);
		Rte_Call_LeftIndicatorClient_Write(STD_LOW);
		////////WRITE_EVENT_TIMESTAMP(Event_15);
		Rte_Call_RightIndicatorClient_Write(STD_LOW);
		////////WRITE_EVENT_TIMESTAMP(Event_15);
		break;	
	default:
		break;//report error
	}
}


void swcFrontLightManagerMainRunnable(void){
	////WRITE_EVENT_TIMESTAMP(Event_4);
	Std_ReturnType ret = RTE_E_NOK;
	uint32 beamState = BEAM_OFF;
	sint8 blinkState = BLINK_OFF;
	LightRequestRecord data;

	//Call the the HeadLight
	////////WRITE_EVENT_TIMESTAMP(Event_8);
	////WRITE_EVENT_TIMESTAMP(Event_4);
	ret = Rte_Read_beamModeReceiver_beamMode(/*OUT*/&beamState);
	//WRITE_EVENT_TIMESTAMP(Event_7);
	if(RTE_E_OK != ret){
		//WRITE_EVENT_TIMESTAMP(Event_7);//report error;
		//WRITE_EVENT_TIMESTAMP(Event_11);
	}	
	else{
		//WRITE_EVENT_TIMESTAMP(Event_7);
		ret = Rte_Write_lightBrightnessSender_lightBrightness(/*OUT*/ beamState);
		//WRITE_EVENT_TIMESTAMP(Event_11);
		if(RTE_E_OK != ret)
			;//report error;
	}
	
	//Turn Other light On
	//WRITE_EVENT_TIMESTAMP(Event_11);
	//WRITE_EVENT_TIMESTAMP(Event_13);
	ret = Rte_Read_lightRequestReceiver_lightRequest(/*OUT*/&data);
	////WRITE_EVENT_TIMESTAMP(Event_7);

	if(RTE_E_OK != ret)
		;//report error;
	else
		turnOtherLightOn(&data);

	////WRITE_EVENT_TIMESTAMP(Event_7);
	ret = Rte_Read_blinkReceiver_blink(/*OUT*/&blinkState);
	////WRITE_EVENT_TIMESTAMP(Event_8);
	if(RTE_E_OK != ret)
		;//report error;
	else
		handleBlinkState(blinkState);
	////WRITE_EVENT_TIMESTAMP(Event_8);;

	/*while(true){
		////WRITE_EVENT_TIMESTAMP(Event_2);
	}*/
		/*while(true){
			//do nothing
			ret = Rte_Read_lightRequestReceiver_lightRequest(&data);
			for(int i=0; i<999;)
				i++;
		}*/
}



// Model initialize function
void swcFrontLightManagerInitRunnable(void){
  //no initialization code required
  ////////WRITE_EVENT_TIMESTAMP(Event_18);
  Rte_Call_RunControl_RequestRUN();
  ////////WRITE_EVENT_TIMESTAMP(Event_18);
}


