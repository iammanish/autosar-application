#include "SwcLightRequest.h"
#include "stdlib.h"
#include "Dio.h"



void swcLightRequestMainRunnable(void){
	//WRITE_EVENT_TIMESTAMP(Event_21);
	uint32 otherLightsSwitchStatus=0;
	uint8 headLightSwitchStatus=0;
	Std_ReturnType ret = RTE_E_NOK;
	LightRequestRecord data;

	memset(&data, 0 ,sizeof(data));
	//data.off = ON;


	//Set the head light
	//WRITE_EVENT_TIMESTAMP(Event_21);
	ret = Rte_Call_OtherLightsSwitchStatusClient_readOtherLightsSwitchStatus(/*OUT*/&otherLightsSwitchStatus);
	////WRITE_EVENT_TIMESTAMP(Event_22);

	if(RTE_E_OK != ret)
		; //report error//otherLightsSwitchStatus = LIGHT_INVALID;
	else{
		//if(otherLightsSwitchStatus)
			setNextOtherLightState(&data, otherLightsSwitchStatus);		
	}

	////WRITE_EVENT_TIMESTAMP(Event_22);
	////WRITE_EVENT_TIMESTAMP(Event_13);
	ret = Rte_Write_lightRequestSender_lightRequest((const LightRequestRecord * )&data);
	////WRITE_EVENT_TIMESTAMP(Event_15);
	////WRITE_EVENT_TIMESTAMP(Event_23);
	
	if(RTE_E_OK != ret)
		;//report error

	//Set other lights
	////WRITE_EVENT_TIMESTAMP(Event_23);
	ret = Rte_Call_HeadLightSwitchStatusClient_readHeadLightSwitchStatus(/*OUT*/&headLightSwitchStatus);
	////WRITE_EVENT_TIMESTAMP(Event_24);

	if(RTE_E_OK != ret)
		; //headLightSwitchStatus = BEAM_INVALID;
	else{
		//if(headLightSwitchStatus)
		////WRITE_EVENT_TIMESTAMP(Event_24);
			ret = Rte_Write_beamModeSender_beamMode(getNextBeamState(headLightSwitchStatus));
			////WRITE_EVENT_TIMESTAMP(Event_25);
		//else
			//ret = Rte_Write_beamModeSender_beamMode(BEAM_OFF);	
	}

	if(RTE_E_OK != ret)
			; //report error;

	//Set the blinking mode
	////WRITE_EVENT_TIMESTAMP(Event_25);
	ret = Rte_Write_blinkSender_blink(getBlinkingMode());	
	////WRITE_EVENT_TIMESTAMP(Event_26);
	////WRITE_EVENT_TIMESTAMP(Event_26);
			/*while(true){
			//do nothing
			;
		}*/
}


void SwcLightRequest_Init(void){
	//do nothing
}

