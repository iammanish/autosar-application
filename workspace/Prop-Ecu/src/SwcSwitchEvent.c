//Includes
#include "SwcSwitchEvent.h"
#include <stdlib.h>

//TODO:Manish
#include "Dio.h"


Std_ReturnType swcHeadLightSwitchStatusRunnable(/*OUT*/uint8 * headLightSwitchStatus){
	//WRITE_EVENT_TIMESTAMP(Event_17);

	DigitalLevel ss=0;
	SignalQuality quality = 0;
	Std_ReturnType ret = RTE_E_DigitalServiceRead_E_NOT_OK;

	//WRITE_EVENT_TIMESTAMP(Event_17);
	ret = Rte_Call_HeadLightSwitchStatusClient_Read(/*OUT*/&ss, /*OUT*/&quality);
	//WRITE_EVENT_TIMESTAMP(Event_18);
	if(RTE_E_DigitalServiceRead_E_OK != ret){
		//TODO: reprt Error
		//WRITE_EVENT_TIMESTAMP(Event_18);
		return RTE_E_SwitchEventLightRequestCSIf2_error_CanNotRead;
	}
	*headLightSwitchStatus = ss;
	//WRITE_EVENT_TIMESTAMP(Event_18);
	return E_OK;
}	



Std_ReturnType swcOtherLightsSwitchStatusRunnable(/*OUT*/uint32 * otherLightsSwitchStatus){
	////WRITE_EVENT_TIMESTAMP(Event_19);
	DigitalLevel ss=0;
	SignalQuality quality = 0;
	Std_ReturnType ret = RTE_E_DigitalServiceRead_E_NOT_OK;

	////WRITE_EVENT_TIMESTAMP(Event_19);
	ret = Rte_Call_OtherLightsSwitchStatusClient_Read(/*OUT*/&ss, /*OUT*/&quality);
	//WRITE_EVENT_TIMESTAMP(Event_20);
	if(RTE_E_DigitalServiceRead_E_OK != ret){
		//TODO: reprt Error
		//WRITE_EVENT_TIMESTAMP(Event_20);
		return RTE_E_SwitchEventLightRequestCSIf1_error_CanNotRead;
	}
	*otherLightsSwitchStatus = ss;
	//WRITE_EVENT_TIMESTAMP(Event_20);
	return E_OK;
}	

void swcSwitchEventInitRunnable(void){
	//Do nothing
}



