
#ifndef _UTILITY_H_
#define _UTILITY_H_

#define ON 1
#define OFF 0

//#define UINT_MAX 255

typedef enum BeamState{
	BEAM_OFF,
	BEAM_HIGH,
	BEAM_LOW,
	BEAM_INVALID
} e_BeamState;

typedef enum OtheLightState{
	ALL_LIGHT_OFF,
	LEFT_INDICATOR_ON,
	RIGHT_INDICATOR_ON,
	PARKING_LIGHT_ON,
	LIGHT_INVALID,
	ALL_BLINK
} e_OtheLightState;

typedef enum BlinkingMode{
	BLINK_OFF,
	BLINK_ON,
	BLINK_INVALID
} e_BlinkingMode;


#endif 
