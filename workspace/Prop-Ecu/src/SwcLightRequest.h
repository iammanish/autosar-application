#ifndef _MainController_h_
#define _MainController_h_


//RTE generated Includes
#include "Rte_SwcLightRequestType.h"
#include "Rte_SwcLightRequestType_Type.h"
#include "Rte_Type.h"
#include "Utility.h"

e_BeamState beamState = BEAM_OFF;
e_OtheLightState otherLightState = ALL_LIGHT_OFF;
e_BlinkingMode blinkState = BLINK_OFF;

e_BeamState getNextBeamState(	bool change){
	if(change){
		switch(beamState){
			case BEAM_OFF: beamState = BEAM_HIGH;
				break;
			case BEAM_HIGH: beamState = BEAM_LOW;
				break;
			case BEAM_LOW: beamState = BEAM_OFF;
				break;
			default:
				beamState = BEAM_INVALID;
			}
	}
	return beamState;
}

void setNextOtherLightState(LightRequestRecord* data, 	bool change){
	if(change){
		switch(otherLightState){
		case ALL_LIGHT_OFF: 		
			otherLightState = LEFT_INDICATOR_ON;			
			break;
		case LEFT_INDICATOR_ON: 
			otherLightState = RIGHT_INDICATOR_ON;
			break;
		case RIGHT_INDICATOR_ON: 
			otherLightState = PARKING_LIGHT_ON;
			break;
		case PARKING_LIGHT_ON: 
			otherLightState = ALL_BLINK;
			break;	
		case ALL_BLINK: 
			otherLightState = ALL_LIGHT_OFF;
			break;		
		default:
			otherLightState = LIGHT_INVALID;
		}
	}

	switch(otherLightState){
		case ALL_LIGHT_OFF: 
			data->off = ON;
			break;
		case LEFT_INDICATOR_ON: 
			data->leftIndicator = ON;
			break;
		case RIGHT_INDICATOR_ON: 
			data->rightIndicator = ON;
			break;
		case PARKING_LIGHT_ON: 
			data->parking = ON;
			break;	
		case ALL_BLINK: 
			//data->off = OFF;
			data->leftIndicator = ON;
			data->rightIndicator = ON;
			data->parking = ON;
			break;		
		default:
			data->off = ON;
		}
}


//This is dummy operation, will be useful in extending the functionality in future.
e_BlinkingMode getBlinkingMode(void){
	return (ALL_BLINK == otherLightState? BLINK_ON : BLINK_OFF);
}
#endif 

